---
layout: markdown_page
title: "GitLab Direction"
---

This page describes the direction and roadmap for GitLab. It's organized from
the short to the long term.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting Merge Requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+Merge+Requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## What our customers want

As a company, GitLab tries to make things that are useful for our customers as
well as ourselves. After all, GitLab is one of the biggest users of GitLab. If a
customer requests a feature, it carries extra weight. Due to our short release
cycle, we can ship simple feature requests, such as an API extension, within one
or two months.

## Previous releases

On our [release list page](/release-list/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Future releases

GitLab releases a new version [every single month on the 22nd]. Note that we
often move things around, do things that are not listed, and cancel things that
_are_ listed.

This page is always in draft, meaning some of the things here might not ever be
in GitLab. New premium features are indicated with "New premium feature" in the
issue title. This is our best estimate of what will be new premium features, but
is in no way definitive.

The list is an outline of **tentpole features** -- the most important features
of upcoming releases -- and doesn't include any contributions from volunteers
outside the company. This is not an authoritative list of upcoming releases - it
only reflects current [milestones](https://gitlab.com/groups/gitlab-org/milestones).

<%= direction %>

[every single month on the 22nd]: /2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab

## Future Premium Features

Below is the list of premium features we are working on. These features will be available
for GitLab Enterprise Edition Premium. The higher the item on the list, the sooner we expect
to ship it.

* [GitLab Geo Disaster Recovery](https://gitlab.com/gitlab-org/gitlab-ee/issues/846)
* [Time Tracking](https://gitlab.com/gitlab-org/gitlab-ee/issues/1293)
* [License Finder](https://gitlab.com/gitlab-org/gitlab-ee/issues/1125)

## Wishlist <a name="wishlist"></a>

Below are features we'd really like to see in GitLab.
This list is not prioritized. We invite everyone to join the discussion by clicking the wishlist item that is of interest to you.
Feel free to comment, vote up or down any issue or just follow the conversation.  For GitLab sales, please add a link to the account in Salesforce.com that has expressed interest in a wishlist feature.
We very much welcome contributions that implement any of these things.

### CI

<%= wishlist["CI"] %>

### Code Review

[Code review meta issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/19049)

<%= wishlist["code review"] %>

### Container Registry

<%= wishlist["container registry"] %>

### Issue Boards

[Issue Boards meta issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/21365)

<%= wishlist["issue boards"] %>

### Issue tracker

<%= wishlist["issues"] %>

### Moderation Tools

[Moderation tools meta issue ](https://gitlab.com/gitlab-org/gitlab-ce/issues/19313)

<%= wishlist["moderation"] %>

### Monitoring

<%= wishlist["monitoring"] %>

### Open Source Projects

[Features for open source projects](https://gitlab.com/gitlab-org/gitlab-ce/issues/8938)

<%= wishlist["open source"] %>

### Pages

<%= wishlist["pages"] %>

### Performance

<%= wishlist["Performance"] %>

### Usability

<%= wishlist["usability"] %>

### User management

[User management meta issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/19860)

<%= wishlist["user management"] %>

### Version Control for Everything

<%= wishlist["vcs for everything"] %>

### Wiki

[Wiki improvements meta issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/19276)

<%= wishlist["wiki"] %>

### Moonshots

- [.git namespace with onename](https://gitlab.com/gitlab-org/gitlab-ce/issues/4232)
<%= wishlist["moonshots"] %>

### New Premium Features

<%= wishlist["ee product"] %>

## Scope <a name="scope"></a>

[Our vision](#vision) is an integrated set of tools for the software development lifecycle based on convention over configuration.
To achieve this we ship the following features in our Omnibus package:

1. **Idea** (Chat) => [Mattermost](http://www.mattermost.org/)
1. **Issue** (Tracker) => Issue Tracker
1. **Plan** (Board) => [Issue Boards](https://about.gitlab.com/solutions/issueboard/)
1. **Code** (IDE) => [Web editor](https://docs.gitlab.com/ce/user/project/repository/web_editor.html) and [web terminal](https://docs.gitlab.com/ce/ci/environments.html#web-terminals)
1. **Commit** (Repo) => GitLab Repositories
1. **Test** (CI) => [GitLab Continuous Integration](https://about.gitlab.com/gitlab-ci/) and [Container Registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/) ship with GitLab, and have an extended [vision for CI and CD](https://about.gitlab.com/direction/cicd/).
1. **Review** (MR) => GitLab Merge Requests and [Review Apps](https://about.gitlab.com/features/review-apps/)
1. **Staging** (CD) => [GitLab Deploy](https://docs.gitlab.com/ce/ci/environments.html) and [auto deploy](https://docs.gitlab.com/ce/ci/autodeploy/index.html)
1. **Production** (Chatops) => [Mattermost slash commands](https://docs.gitlab.com/ee/project_services/mattermost_slash_commands.html) and [Slack slash commands](https://docs.gitlab.com/ce/project_services/slack_slash_commands.html)
1. **Feedback** (Monitoring) => [Cycle Analytics](https://about.gitlab.com/solutions/cycle-analytics/) ships with GitLab, in the future we plan to ship [application monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/23841) by bundling [Prometheus](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1481).

Also see our video [In 13 minutes from Kubernetes to a complete application development tool](https://about.gitlab.com/2016/11/14/idea-to-production/).

### Outside our scope

1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)
1. **Container Scheduler** although we do want to use [GitLab Deploy](https://gitlab.com/gitlab-org/gitlab-ce/issues/3286) to deploy to CloudFoundry, OpenStack, OpenShift, Kubernetes, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Configuration management** although we do want to upload cookbooks, manifests, playbooks, and modules for respectively Chef, Puppet, Ansible, and Salt.
1. **Container configuration agents** [ContainerPilot](http://container-solutions.com/containerpilot-on-mantl/), [Orchestrator-agent](https://www.percona.com/blog/2016/04/13/orchestrator-agent-how-to-recover-a-mysql-database/)
1. **Distributed configuration stores** Consul, Etcd, Zookeeper, Vault
1. **Logging** [Fluentd](http://www.fluentd.org/architecture), ELK stack, Graylog, Splunk, [LogDNA](https://logdna.com/)
1. **Error monitoring** Sentry, Airbrake, Bugsnag
1. **Network** [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Tracing** OpenTracing, [LightStep](http://lightstep.com/)
1. **Network security** Nmap, rkhunter, Metasploit, Snort, OpenVAS, OSSEC

## Vision <a name="vision"></a>

See below for a TL;DR on how and what we're building at GitLab.
[See this page for an in-depth in our product strategy](/direction/strategy).

---

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized. An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, checked and documented. Stitching all these stages of the software developement lifecycle together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that an **integrated set of tools for the software development lifecycle based on convention over configuration** offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from idea to production**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.

We prefer to offer an integrated set of tools instead of a network of services or offering plugins for the following reasons:

1. We think an integrated set of tools provides a better user experience than a modular approach, as detailed by [this article from Stratechery](https://stratechery.com/2013/clayton-christensen-got-wrong/).
1. The open source nature of GitLab ensures that that we can combine great open source products.
1. Everyone can contribute to create a feature set that is [more complete than other tools](https://about.gitlab.com/comparison/). We'll focus on making all the parts work well together to create a better user experience.
1. Because GitLab is open source the enhancements can become [part of
the codebase instead](http://doc.gitlab.com/ce/project_services/project_services.html) of being external. This ensures the automated tests for all
functionality are continually run, ensuring that additions keep working work. This is contrast to externally maintained plugins that might not be updated.
1. Having the enhancements as part of the codebase also
ensures GitLab can continue to evolve with its additions instead of being bound
to an API that is hard to change and that resists refactoring. Refactoring is essential to maintaining a codebase that is easy to contribute to.
1. Many people use GitLab on-premises, for such situations it is much easier to install one tool than installing and integrating many tools.
1. GitLab is used by many large organizations with complex purchasing processes, having to buy only one subscription simplifies their purchasing.
